#pragma once
#include <utility>
#include <memory>
#include <string>
#include <string_view>
#include <tuple>
#include <cstdint>


namespace auteur {


class SceneBase {
public:
  using flag_type = std::size_t;

  constexpr static flag_type s_load  = 0x01u;
  constexpr static flag_type s_start = 0x02u;
  constexpr static flag_type s_pause = 0x04u;

  // Aliases for the above flags which read more easily.
  constexpr static flag_type s_loaded  = s_load;
  constexpr static flag_type s_started = s_start;
  constexpr static flag_type s_paused  = s_pause;

  inline explicit SceneBase(std::string_view id): m_id{id}, m_flags{0u} {}

  SceneBase(SceneBase const&) = delete;
  SceneBase& operator=(SceneBase const&) = delete;

  inline SceneBase(SceneBase&& other) noexcept
    : m_id{std::move(other.m_id)}
    , m_flags(std::exchange(other.m_flags, 0u))
  { }

  SceneBase& operator=(SceneBase&& other) noexcept {
    m_id = std::move(other.m_id);
    m_flags = std::exchange(other.m_flags, 0u);
    return *this;
  }

  virtual ~SceneBase() noexcept = default;

  inline std::string_view id() const noexcept { return m_id; }
  inline flag_type flags() const noexcept { return m_flags; }

  inline bool is(flag_type flag) const noexcept {
    return 0 != (m_flags & flag);
  }
  inline bool is_not(flag_type flag) const noexcept {
    return 0 == (m_flags & flag);
  }

  inline bool loaded() const noexcept { return m_flags & s_load; }
  inline bool running() const noexcept { return m_flags & s_start; }
  inline bool paused() const noexcept { return m_flags & s_pause; }

protected:
  inline void add_flags(flag_type flag) noexcept { m_flags |= flag; }
  inline void remove_flags(flag_type flag) noexcept { m_flags &= ~flag; }
  inline void set_flags(flag_type flags) noexcept { m_flags = flags; }

  virtual void do_load();
  virtual void do_unload();
  virtual void do_start();
  virtual void do_stop();
  virtual void do_pause();
  virtual void do_unpause();

  std::string m_id;
  flag_type m_flags;
};


} // namespace auteur
