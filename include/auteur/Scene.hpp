#pragma once
#include "auteur/SceneBase.hpp"


namespace auteur {

/**
 * A default scene class which injects no behaviour around the virtual methods.
 *
 * This class does guard against repeat operations.
 *
 * If you want to add behaviour such as logging around the virtual SceneBase methods,
 * override that class directly instead.
 */
class Scene: public SceneBase {
public:
  using SceneBase::SceneBase;

  inline void load() {
    if (!loaded()) {
      do_load();
      add_flags(s_loaded);
    }
  }

  inline void unload() {
    stop();
    if (loaded()) {
      do_unload();
      remove_flags(s_loaded);
    }
  }

  inline void start() {
    load();
    if (!running()) {
      do_start();
      add_flags(s_started);
    }
  }

  inline void stop() {
    if (running()) {
      do_stop();
      remove_flags(s_started);
    }
  }

  inline void pause() {
    if (!paused()) {
      do_pause();
      add_flags(s_paused);
    }
  }

  inline void unpause() {
    if (paused()) {
      do_unpause();
      remove_flags(s_paused);
    }
  }
};

} // namespace auteur
