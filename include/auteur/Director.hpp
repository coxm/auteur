#pragma once
#include <utility>
#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <string_view>
#include <type_traits>
#include <stdexcept>

#include "./TransitionHandler.hpp"
#include "./exceptions.hpp"


namespace auteur {


class DuplicateScene: std::logic_error {
public:
  using std::logic_error::logic_error;

  inline DuplicateScene(std::string_view scene_id)
    : std::logic_error{std::string(scene_id)}
  { }
};

class NoSuchScene: std::out_of_range {
public:
  using std::out_of_range::out_of_range;

  inline NoSuchScene(std::string_view scene_id)
    : std::out_of_range{std::string(scene_id)}
  { }
};


template <typename SceneT, typename TransitionHandler = TransitionHandler>
class Director {
public:
  using scene_type = SceneT;
  using transition_handler_type = TransitionHandler;
  using scene_ptr = std::unique_ptr<scene_type>;
  using container_type = std::vector<scene_ptr>;

  inline Director()
    : m_tuple{}
    , m_current{-1}
    , m_previous{-1}
  {
  }

  template <typename... TransitionHandlerArgs>
  inline Director(
    container_type&& container,
    TransitionHandlerArgs&&... transition_handler_args
  )
    : m_tuple{
        std::move(container),
        {std::forward<TransitionHandlerArgs>(transition_handler_args)...}
      }
    , m_current{-1}
    , m_previous{-1}
  { }

  template <typename... TransitionHandlerArgs>
  inline Director(TransitionHandlerArgs&&... transition_handler_args)
    : m_tuple{
        container_type{},
        {std::forward<TransitionHandlerArgs>(transition_handler_args)...}
      }
    , m_current{-1}
    , m_previous{-1}
  { }

  Director(Director const&) = delete;
  Director& operator=(Director const&) = delete;

  inline Director(Director&& other) noexcept
    : m_tuple{std::move(other.m_tuple)}
    , m_current(std::move(other.m_current))
    , m_previous(std::move(other.m_previous))
  { }

  inline Director& operator=(Director&& other) noexcept {
    m_tuple = std::move(other.m_tuple);
    m_current = std::move(other.m_current);
    m_previous = std::move(other.m_previous);
    return *this;
  }

  inline void reset(std::string_view id) {
    auto& items = scenes();
    auto it = std::find_if(items.begin(), items.end(), [id] (auto const& ptr) {
      return ptr->id() == id;
    });
    m_current = it - items.begin();
    m_previous = -1;
  }

  inline std::size_t scene_count() const noexcept { return scenes().size(); }

  inline std::string_view current_id() const { return current().id(); }

  inline scene_type& current() { return *scenes()[m_current]; }
  inline scene_type const& current() const { return *scenes()[m_current]; }

  template <typename T>
  requires std::is_base_of_v<scene_type, T>
  inline T& current() {
    return dynamic_cast<T&>(current());
  }

  template <typename T>
  requires std::is_base_of_v<scene_type, T>
  inline T const& current() const {
    return dynamic_cast<T&>(current());
  }

  inline transition_handler_type& transition_handler() noexcept {
    return std::get<transition_handler_type>(m_tuple);
  }
  inline transition_handler_type const& transition_handler() const noexcept {
    return std::get<transition_handler_type>(m_tuple);
  }

  inline bool has(std::string_view id) const {
    auto& items = scenes();
    auto it = find_iter(id);
    return it != items.end();
  }

  scene_type& add(scene_ptr&& scene) {
    std::string_view id = scene->id();
    if (has(id)) {
      throw DuplicateScene(id);
    }
    return *scenes().emplace_back(std::move(scene));
  }

  template <typename DerivedScene, typename... Args>
  requires std::is_base_of_v<scene_type, DerivedScene>
  DerivedScene& add(Args&& ...args) {
    auto uptr = std::make_unique<DerivedScene>(std::forward<Args>(args)...);
    auto& scene = *uptr;
    add(std::move(uptr));
    return scene;
  }

  inline scene_type* find(std::string_view id) {
    auto& items = scenes();
    auto it = find_iter(id);
    return it == items.end() ? nullptr : **it;
  }

  inline scene_type const* find(std::string_view id) const {
    auto& items = scenes();
    auto it = find_iter(id);
    return it == items.end() ? nullptr : **it;
  }

  inline scene_type& at(std::string_view id) {
    auto& items = scenes();
    auto it = find_iter(id);
    if (it == items.end()) {
      throw NoSuchScene(id);
    }
    return **it;
  }

  inline scene_type const& at(std::string_view id) const {
    auto& items = scenes();
    auto it = find_iter(id);
    if (it == items.end()) {
      throw NoSuchScene(id);
    }
    return **it;
  }

  template <typename T>
  requires std::is_base_of_v<scene_type, T>
  T& at(std::string_view id) {
    return dynamic_cast<T&>(at(std::string(id)));
  }

  template <typename T>
  requires std::is_base_of_v<scene_type, T>
  T const& at(std::string_view id) const {
    return dynamic_cast<T&>(at(std::string(id)));
  }

  template <typename... TransitionArgs>
  inline void
  transition(std::string_view id, TransitionArgs&& ...targs) {
    auto& items = scenes();
    auto it = find_iter(id);
    if (it == items.end()) {
      throw NoSuchScene(id);
    }
    transition_impl(
      m_current,
      it - items.begin(),
      std::forward<TransitionArgs>(targs)...
    );
  }

  template <typename... TransitionArgs>
  void revert(TransitionArgs&& ...targs) {
    if (m_previous == -1) {
      throw InvalidRevertError("No previous scene");
    }
    transition_impl(m_current, m_previous, std::forward<TransitionArgs>(targs)...);
  }

  inline container_type& scenes() noexcept {
    return std::get<container_type>(m_tuple);
  }
  inline container_type const& scenes() const noexcept {
    return std::get<container_type>(m_tuple);
  }

private:
  using iterator = typename container_type::iterator;

  constexpr static int s_invalid_index = -1;

  auto find_iter(std::string_view id) {
    auto& items = scenes();
    return std::find_if(items.begin(), items.end(), [id] (auto const& ptr) {
      return ptr->id() == id;
    });
  }

  auto find_iter(std::string_view id) const {
    auto& items = scenes();
    return std::find_if(items.cbegin(), items.cend(), [id] (auto const& ptr) {
      return ptr->id() == id;
    });
  }

  template <typename... TransitionArgs>
  inline void
  transition_impl(int prev, int next, TransitionArgs&& ...args) {
    auto data = scenes().data();
    transition_handler().transition(
      data[prev].get(),
      data[next].get(),
      std::forward<TransitionArgs>(args)...
    );
    m_current = next;
    m_previous = prev;
  }

  std::tuple<container_type, transition_handler_type> m_tuple;
  int m_current;
  int m_previous;
};


} // namespace auteur
