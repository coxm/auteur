#include <stdexcept>


namespace auteur {

class InvalidRevertError: std::logic_error {
public:
  using std::logic_error::logic_error;
};

} // namespace auteur
