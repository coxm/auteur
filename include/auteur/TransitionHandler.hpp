namespace auteur {


struct TransitionHandler {
  enum class Action: unsigned char {
    /// Leave the scene unchanged.
    none,
    /// Pause the scene.
    pause,
    /// Unpause the scene.
    unpause,
    /// Start the scene.
    start,
    /// Stop the scene (resuming will restart it from the beginning).
    stop,
    /// Stop and unload the scene.
    unload
  };

  template <typename SceneT>
  static inline void act(SceneT* scene, Action type) {
    switch (type) {
      case Action::none:
        break;
      case Action::pause:
        scene->pause();
        break;
      case Action::unpause:
        scene->unpause();
        break;
      case Action::start:
        scene->start();
        break;
      case Action::stop:
        scene->stop();
        break;
      case Action::unload:
        scene->unload();
        break;
    }
  }

  template <typename SceneT>
  inline void transition(
    SceneT* prev,
    SceneT* next,
    Action prevAction = Action::stop,
    Action nextAction = Action::start
  ) {
    act(prev, prevAction);
    act(next, nextAction);
  }
};


} // namespace auteur
