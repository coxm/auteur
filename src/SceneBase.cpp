#include "auteur/Scene.hpp"


namespace auteur {

void SceneBase::do_load() {}
void SceneBase::do_unload() {}
void SceneBase::do_start() {}
void SceneBase::do_stop() {}
void SceneBase::do_pause() {}
void SceneBase::do_unpause() {}

} // namespace auteur
