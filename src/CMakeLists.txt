add_library(auteur SceneBase.cpp)
add_library(auteur::auteur ALIAS auteur)
target_include_directories(auteur
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
set_target_properties(auteur PROPERTIES
  VERSION "${PROJECT_VERSION}"
  SOVERSION "${PROJECT_VERSION}"
  PROJECT_LABEL "auteur"
  DEBUG_POSTFIX d)
target_compile_options(auteur PRIVATE
  $<$<CXX_COMPILER_ID:GNU>:
    -Wall -Weffc++ -Werror -Wshadow -Wold-style-cast -Woverloaded-virtual
  >
  $<$<CXX_COMPILER_ID:Clang>:
    -Wall -Werror -Wshadow -Wold-style-cast -Woverloaded-virtual
  >
)
target_compile_features(auteur PUBLIC cxx_std_17)
