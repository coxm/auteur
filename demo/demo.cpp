#include <functional>
#include <iostream>

#include <auteur/Scene.hpp>
#include <auteur/Director.hpp>


using Scene = auteur::Scene;
using Director = auteur::Director<Scene>;
using Action = auteur::TransitionHandler::Action;


class MenuScene: public Scene { using Scene::Scene; };
class TitleScene: public Scene { using Scene::Scene; };
class BossScene: public Scene {
public:
  BossScene(std::string_view id, int boss_hp): Scene{id}, boss_health{boss_hp} { }

  int const boss_health;
};


template <typename OS>
inline OS& operator<<(OS& os, Scene const& scene) {
  return os
    << '[' << scene.id() << ": "
    << (scene.loaded() ? "loaded, " : "unloaded, ")
    << (scene.running() ? "running, " : "stopped, ")
    << (scene.paused() ? "paused" : "unpaused")
    << ']';
}


int main() {
  // Create the director with an initial scene, and start immediately.
  // Alternatively, use director.reset() to set the first scene after construction.
  Director director;
  director.add<TitleScene>("Title");
  director.reset("Title");
  director.current().start();

  director.add<MenuScene>("MainMenu");
  director.add<MenuScene>("OtherMenu");
  director.add<BossScene>("SmallBoss", 100);
  director.add<BossScene>("BigBoss", 1000);

  auto print_state = [&director](std::string_view stage) {
    std::cout << stage << "\n  " << "Current: " << director.current_id() << "\n  ";
    for (auto const& scene: director.scenes()) {
      std::cout << scene << std::endl;
    }
  };

  // Now initialisation is complete, start the main menu.
  director.current().start(); // Automatically loads the scene.
  director.at("SmallBoss").load(); // Preload SmallBoss.
  print_state("\nStarted at main menu");

  // Change the scene to SmallBoss, stopping the main menu.
  director.transition("SmallBoss");
  print_state("\nTransition 1");

  // Pause the current scene, and transition to the main menu.
  director.transition("MainMenu", Action::pause);
  print_state("\nTransition 2");

  // Resume current scene, pausing MainMenu.
  director.revert(Action::pause); // Or director.transition("BigBoss", Action::pause);
  print_state("\nTransition 3");

  // Stop and unload current scene, then return to main menu.
  director.transition("MainMenu", Action::unload);

  return 0;
}
