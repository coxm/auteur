#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include <auteur/Director.hpp>
#include <auteur/Scene.hpp>


using Scene = auteur::Scene;

class Menu: public Scene {
public:
  using Scene::Scene;
};

class Level: public Scene {
public:
  using Scene::Scene;
};

struct TransitionCounter {
  Scene* m_lastSrc = nullptr;
  Scene* m_lastDest = nullptr;
  int m_numTransitions = 0;
  int m_lastArg = -1;

  void transition(Scene* src, Scene* dest, int arg) {
    m_lastSrc = src;
    m_lastDest = dest;
    ++m_numTransitions;
    m_lastArg = arg;
  }
};

using Director = auteur::Director<Scene, TransitionCounter>;


SCENARIO("Using a Director", "[auteur][Director]") {
  GIVEN("a Director") {
    Director director;
    director.add(std::make_unique<Menu>("mainmenu"));
    REQUIRE(director.scene_count() == 1);
    director.add<Level>("level0");
    REQUIRE(director.scene_count() == 2);
    director.add<Level>("level1");
    REQUIRE(director.scene_count() == 3);

    director.reset("mainmenu");

    REQUIRE(director.transition_handler().m_numTransitions == 0);

    THEN("we can access the current scene") {
      CHECK(director.current_id() == "mainmenu");
    }
    THEN("we can access the scenes via at()") {
      CHECK(director.at<Scene>("mainmenu").id() == "mainmenu");
      CHECK(director.at<Scene>("level0").id() == "level0");
    }

    REQUIRE(director.transition_handler().m_numTransitions == 0);

    WHEN("we transition to another scene") {
      director.transition("level0", 0xBEEF);

      THEN("the current scene is updated") {
        CHECK(director.current_id() == "level0");
      }

      THEN("the transition handler is called with the scenes") {
        auto const& handler = director.transition_handler();
        CHECK(handler.m_numTransitions == 1);
        CHECK(handler.m_lastArg == 0xBEEF);
        CHECK(handler.m_lastSrc->id() == "mainmenu");
        CHECK(handler.m_lastDest->id() == "level0");
      }
    }
  }
}
