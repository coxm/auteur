#include <catch2/catch_test_macros.hpp>

#include "auteur/Scene.hpp"


using TestScene = auteur::Scene;


void testMoved(
  TestScene const& newScene,
  TestScene const& oldScene,
  std::string_view id
) {
  THEN("the new scene has the correct ID") {
    CHECK(newScene.id() == id);
  }
  THEN("the new scene has the old scene's flags") {
    CHECK(newScene.flags() == (
      TestScene::s_loaded | TestScene::s_started
    ));
  }
  THEN("the old scene has its flags reset") {
    CHECK(oldScene.flags() == 0u);
  }
}


SCENARIO("Using Scenes", "[auteur][Scene]") {
  GIVEN("a Scene") {
    TestScene scene{"test"};

    THEN("the scene has an ID") {
      CHECK(scene.id() == "test");
    }
    THEN("the scene has the correct initial flags") {
      CHECK(scene.flags() == 0);
      CHECK(!scene.running());
      CHECK(!scene.loaded());
    }

    WHEN("the scene has some state") {
      // Modify the scene so we can check its state was moved & reset.
      scene.load();
      scene.start();
      // Ensure the original scene is in the state we expect.
      REQUIRE(scene.loaded());
      REQUIRE(scene.running());
      REQUIRE(scene.flags() == (
        TestScene::s_loaded | TestScene::s_started
      ));

      WHEN("we move-construct from it") {
        TestScene second(std::move(scene));
        testMoved(second, scene, "test");
      }
      WHEN("we move-assign to it") {
        TestScene second{"second"};
        second = std::move(scene);
        testMoved(second, scene, "test");
      }
    }

    WHEN("we load()") {
      scene.load();
      THEN("the scene is loaded") {
        CHECK(scene.loaded());
        CHECK(0 != (scene.flags() & scene.s_loaded));
      }

      WHEN("we unload()") {
        scene.unload();
        THEN("the scene is not loaded") {
          CHECK(!scene.loaded());
          CHECK(0 == (scene.flags() & scene.s_loaded));
        }
      }
    }

    WHEN("we start()") {
      scene.load();
      scene.start();

      THEN("the scene is running") {
        REQUIRE(scene.running());
        REQUIRE(0 != (scene.flags() & scene.s_started));
      }

      WHEN("we stop()") {
        scene.stop();

        THEN("the scene is not running") {
          REQUIRE(!scene.running());
          REQUIRE(0 == (scene.flags() & scene.s_started));
        }

        WHEN("we unload()") {
          scene.unload();

          THEN("the scene is no longer loaded") {
            CHECK(!scene.loaded());
            CHECK(0 == (scene.flags() & scene.s_loaded));
          }
        }
      }
    }
  }
}
